import { Link } from "react-router-dom";
import React from "react";

function TransactionHomeItem({ title, onClick, icon, style, path, type }) {
	return (
		<Link
			to={path ? path : "/"}
			className={`flex flex-col items-center justify cursor-pointer ${style}`}
		>
			<div className="w-[40px] h-[40px] bg-green-500 flex flex-col items-center justify-center rounded-full relative">
				{type == "ci" ? (
					icon
				) : (
					<img src={icon} className="w-[24] h-[24]" />
				)}
			</div>
			<p className="mt-8 font-Regular  text-gray-800 text-13 text-center">
				{title}
			</p>
		</Link>
	);
}

export default TransactionHomeItem;
