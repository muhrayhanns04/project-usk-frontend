import Cookies from "js-cookie";
import { PURE_URL } from "../helpers/Server";
import React from "react";
import { SpinnerCircularFixed } from "spinners-react";
import WALLET from "../assets/icons/transaction/wallet.svg";
import axios from "axios";
import { formatNumberToRupiah } from "../helpers/Common";
import moment from "moment";

function TransferItem({ item, index }) {
  const [isLoading, setIsLoading] = React.useState(false);
  const toggleDownloadInvoice = async () => {
    try {
      const token = Cookies.get("@token");
      await axios.get(`${PURE_URL}/invoice/${item?.id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
        },
      });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div
      className="flex flex-col py-13 px-13 bg-gray-50 w-90% rounded-8 mb-13"
      key={index}
      onClick={() => {
        alert("Menunggu file sedang dipersiapkan...");
        toggleDownloadInvoice();
      }}
    >
      <div className="flex flex-row align-top mb-16">
        <img src={WALLET} className="w-24 h-24 mr-8" alt="" />
        <div>
          <p className="font-semibold text-13 text-gray-800 mb-4">
            {item?.invoice_id}
          </p>
          <div className="flex flex-row items-center">
            <p className="font-semibold text-green-500 mr-8">Transfer</p>
            <p className="font-semibold text-13 text-gray-300 mr-8">
              {moment(item?.created_at).format("d MMMM YYYY")}
            </p>
            <p
              className={`font-semibold text-13 mr-8 text-gray-800 mb-4 ${
                item?.status == 0
                  ? "text-yellow-500"
                  : item?.status == 1
                  ? "text-red-500"
                  : "text-green-500"
              }`}
            >
              {item?.status == 0
                ? "Menunggu"
                : item?.status == 1
                ? "Ditolak"
                : "Selesai"}
            </p>
          </div>
        </div>
      </div>

      <div className="flex flex-row items-center justify-between">
        <div>
          <p className="font-semibold text-13 text-gray-800 mb-4">
            Total Belanja
          </p>
          <p className="font-Bold text-16 text-gray-800">
            {formatNumberToRupiah(item?.cash_request ? item?.cash_request : 0)}
          </p>
        </div>

        <button className="mt-16 px-[27px] py-4 rounded-full bg-green-500 focus:ring-4 ring-green-200 flex flex-col justify-center items-center">
          {isLoading ? (
            <SpinnerCircularFixed
              size={16}
              thickness={180}
              speed={180}
              color="#36ad47"
              secondaryColor="rgba(0, 0, 0, 0.44)"
            />
          ) : (
            <h6 className="font-SemiBold text-13 my-8 text-gray-100">
              Invoice
            </h6>
          )}
        </button>
      </div>
    </div>
  );
}

export default TransferItem;
