import { Navigate, Outlet, useLocation } from "react-router-dom";

import Cookies from "js-cookie";

function RequireAuth({ children }) {
	const token = Cookies.get("@token");
	let location = useLocation();

	if (!token) {
		return <Navigate to="/login" state={{ from: location }} replace />;
	}

	return <Outlet />;
}

export default RequireAuth;
