import { BASE_URL } from "../../helpers/Server";
import Cookies from "js-cookie";
import MINUS from "../../assets/icons/transaction/minus.svg";
import PLACEHOLDER from "../../assets/icons/placeholder/product-image-placeholder.svg";
import PLUS from "../../assets/icons/transaction/plus.svg";
import React from "react";
import { SpinnerCircularFixed } from "spinners-react";
import Swal from "sweetalert2";
import { TransactionHooks } from "../../hooks/transactionhooks";
import axios from "axios";
import { formatNumberToRupiah } from "../../helpers/Common";

function CartItem({ item, index, getCartProducts, getTotalPayment }) {
	const [isCount, setIsCount] = React.useState(false);
	const [isLoading, setIsLoading] = React.useState(false);
	const [qty, setQty] = React.useState(1);

	const { getCartCount } = TransactionHooks();

	const handleDelete = async () => {
		try {
			setIsLoading(true);

			const token = Cookies.get("@token");
			const response = await axios.post(
				`${BASE_URL}/transaction/order/delete/${item?.id}`,
				{ qty: qty },
				{
					headers: {
						Authorization: `Bearer ${token}`,
						Accept: "application/json",
					},
				}
			);

			const Toast = Swal.mixin({
				toast: true,
				position: "top-end",
				showConfirmButton: false,
				timer: 1000,
				timerProgressBar: true,
				didOpen: (toast) => {
					toast.addEventListener("mouseenter", Swal.stopTimer);
					toast.addEventListener("mouseleave", Swal.resumeTimer);
				},
			});

			Toast.fire({
				icon: "success",
				title: `Berhasil Menghapus Produk ke dalam cart`,
			});

			setTimeout(() => {
				setQty(1);
				setIsLoading(false);
			}, 500);

			getCartProducts();
			getTotalPayment();
		} catch (error) {
			setTimeout(() => {
				setIsLoading(false);
			}, 500);

			Swal.fire("Info?", `${error?.response?.data?.message}`, "question");
		}
	};

	return (
		<div
			className="border-gray-10 border p-13 w-50% flex-col justify-center items-center max-h-[345px]"
			key={index}
		>
			<img
				src={item?.product?.image ? item?.product?.image : PLACEHOLDER}
				className="w-[178px] h-[178px] rounded-16"
				alt={item?.product?.name}
			/>
			<div className="mb-16">
				<h6 className="font-SemiBold text-16 my-8 line-clamp-2">
					{item?.product?.name}
				</h6>
				<h6 className="font-Medium text-13 text-gray-500 mb-8">
					{item?.product?.qty} pcs
				</h6>
				<h6 className="font-SemiBold text-16 mb-8">
					{formatNumberToRupiah(item?.product?.total)}
				</h6>
			</div>

			{isCount ? (
				<div className="flex flex-row justify-evenly items-center">
					<button
						onClick={() => {
							if (qty > 1) {
								setQty(qty - 1);
							} else {
								setQty(1);
							}
						}}
						className="rounded-full bg-green-500 focus:ring-4 py-4 ring-green-200 w-[34px] h-[34px] flex-col flex justify-center items-center"
					>
						<img src={MINUS} className="w-16 h-16" alt="" />
					</button>
					<p className="font-Medium text-13 text-gray-500">{qty}</p>
					<button
						onClick={() => setQty(qty + 1)}
						className="rounded-full bg-green-500 focus:ring-4 py-4 ring-green-200 w-[34px] h-[34px] flex-col flex justify-center items-center"
					>
						<img src={PLUS} className="w-16 h-16" alt="" />
					</button>
				</div>
			) : null}

			<button
				className="mt-16 rounded-full bg-red-100 focus:ring-4 h-[38px] w-full ring-red-100 flex flex-col justify-center items-center"
				onClick={() => {
					handleDelete().then(() => getCartCount());
				}}
			>
				{isLoading ? (
					<SpinnerCircularFixed
						size={16}
						thickness={180}
						speed={180}
						color="#36ad47"
						secondaryColor="rgba(0, 0, 0, 0.44)"
					/>
				) : (
					<h6 className="font-SemiBold text-13 my-8 text-red-500">
						Delete
					</h6>
				)}
			</button>
		</div>
	);
}

export default CartItem;
