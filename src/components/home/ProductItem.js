import { BASE_URL } from "../../helpers/Server";
import Cookies from "js-cookie";
import MINUS from "../../assets/icons/transaction/minus.svg";
import PLACEHOLDER from "../../assets/icons/placeholder/product-image-placeholder.svg";
import PLUS from "../../assets/icons/transaction/plus.svg";
import React from "react";
import { SpinnerCircularFixed } from "spinners-react";
import Swal from "sweetalert2";
import { TransactionHooks } from "../../hooks/transactionhooks";
import axios from "axios";
import { formatNumberToRupiah } from "../../helpers/Common";

function ProductItem({ item, getProducts }) {
  const [isCount, setIsCount] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [qty, setQty] = React.useState(1);

  const { getCartCount } = TransactionHooks();

  const handleAddToCart = async () => {
    try {
      getCartCount();
      setIsLoading(true);
      const token = Cookies.get("@token");
      await axios.post(
        `${BASE_URL}/transaction/order/${item?.id}`,
        { qty: qty },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
          },
        }
      );

      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 1000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener("mouseenter", Swal.stopTimer);
          toast.addEventListener("mouseleave", Swal.resumeTimer);
        },
      });

      Toast.fire({
        icon: "success",
        title: `Berhasil Menambahkan Produk(${qty}) ke dalam cart`,
      });

      getProducts();

      setTimeout(() => {
        setQty(1);
        setIsLoading(false);
      }, 500);
    } catch (error) {
      setTimeout(() => {
        setIsLoading(false);
      }, 500);

      Swal.fire("Info?", `${error?.response?.data?.message}`, "question");
    }
  };

  return (
    <div className="border-gray-100 border p-13 w-50% flex-col justify-center items-center mb-13 max-h-[382px]">
      <img
        src={item?.image ? item?.image : PLACEHOLDER}
        className="w-[178px] h-[178px] rounded-16"
        alt={item?.name}
      />
      <div className="mb-16">
        <h6 className="font-SemiBold text-16 my-8 line-clamp-2">
          {item?.name}
        </h6>
        <h6 className="font-Medium text-13 text-gray-500 mb-8">
          {item?.stock} pcs
        </h6>
        <h6 className="font-SemiBold text-16 mb-8">
          {formatNumberToRupiah(item?.price)}
        </h6>
      </div>

      {isCount ? (
        <div className="flex flex-row justify-evenly items-center">
          <button
            onClick={() => {
              if (qty > 1) {
                setQty(qty - 1);
              } else {
                setQty(1);
              }
            }}
            className="rounded-full bg-green-500 focus:ring-4 py-4 ring-green-200 w-[34px] h-[34px] flex-col flex justify-center items-center"
          >
            <img src={MINUS} className="w-16 h-16" alt="" />
          </button>
          <p className="font-Medium text-13 text-gray-500">{qty}</p>
          <button
            onClick={() => setQty(qty + 1)}
            className="rounded-full bg-green-500 focus:ring-4 py-4 ring-green-200 w-[34px] h-[34px] flex-col flex justify-center items-center"
          >
            <img src={PLUS} className="w-16 h-16" alt="" />
          </button>
        </div>
      ) : null}

      <button
        className="mt-16 rounded-full bg-green-500 focus:ring-4 h-[38px] w-full ring-green-200 flex flex-col justify-center items-center"
        onClick={() => {
          setIsCount(!isCount);

          if (isCount) {
            handleAddToCart().then(() => getCartCount());
          }
        }}
      >
        {isLoading ? (
          <SpinnerCircularFixed
            size={16}
            thickness={180}
            speed={180}
            color="#36ad47"
            secondaryColor="rgba(0, 0, 0, 0.44)"
          />
        ) : (
          <h6 className="font-SemiBold text-13 my-8 text-gray-100">
            {isCount ? "Beli" : "Pilih"}
          </h6>
        )}
      </button>
    </div>
  );
}

export default ProductItem;
