import Logo from "../assets/icons/billfold-logo.svg";
import React from "react";

function Layout() {
	return (
		<div className="px-16 pt-16 max-w-sm">
			<img src={Logo} alt="" />
		</div>
	);
}

export default Layout;
