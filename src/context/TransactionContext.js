import React from "react";

export const TransactionContext = React.createContext();

const TransactionProvider = ({ children }) => {
	const [cartCount, setCartCount] = React.useState(0);

	return (
		<TransactionContext.Provider
			value={{
				cartCount,
				setCartCount,
			}}
		>
			{children}
		</TransactionContext.Provider>
	);
};

export default TransactionProvider;
