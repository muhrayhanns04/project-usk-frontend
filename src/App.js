import "./App.css";

import {
  Link,
  Route,
  Routes,
  useLocation,
  useNavigate,
} from "react-router-dom";

import { BASE_URL } from "./helpers/Server";
import CART from "./assets/icons/home/cart.svg";
import CartPage from "./pages/home/CartPage";
import Cookies from "js-cookie";
import HistoryTransactionPage from "./pages/wallet/HistoryTransactionPage";
import HomePage from "./pages/home/HomePage";
import LOGO from "./assets/icons/billfold-logo-medium.svg";
import LoginPage from "./pages/auth/LoginPage";
import React from "react";
import RequireAuth from "./components/RequireAuth";
import Swal from "sweetalert2";
import TopupPage from "./pages/wallet/TopupPage";
import { TransactionHooks } from "./hooks/transactionhooks";
import TransferPage from "./pages/wallet/TransferPage";
import WithdrawalPage from "./pages/wallet/WithdrawalPage";
import axios from "axios";
import ResetPasswordPage from "./pages/auth/ResetPasswordPage";
import USER from "./assets/icons/auth/user.svg";

function App() {
  let location = useLocation();
  const navigate = useNavigate();

  const { getCartCount, cartCount } = TransactionHooks();

  const handleLogout = async () => {
    try {
      const token = Cookies.get("@token");
      await axios.post(
        `${BASE_URL}/logout`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
          },
        }
      );

      Cookies.remove("@token");
      Cookies.remove("@id");
      return navigate("/", { replace: true });
    } catch (error) {
      Cookies.remove("@token");
      Cookies.remove("@id");
      Swal.fire("Info?", `${error?.response?.data?.message}`, "question");
      return navigate("/", { replace: true });
    }
  };

  React.useEffect(() => {
    getCartCount();
  }, [cartCount]);

  return (
    <div>
      {location.pathname !== "/login" ? (
        <div className="flex flex-col justify-center items-center md:bg-green-500 w-full md:h-[100vh]">
          <div className="flex flex-col w-90% md:w-[414px] bg-white py-16 md:px-8">
            <div className="flex flex-row justify-between items-center">
              <Link to="/">
                <img src={LOGO} alt="Billfold Indonesia" />
              </Link>

              <div className="flex flex-row items-center">
                <div className="relative mr-16">
                  <div className="flex flex-row items-center">
                    <Link to="/reset ">
                      <img
                        src={USER}
                        className="w-[32px] h-[32px] mr-8"
                        alt="Billfold Indonesia"
                      />
                    </Link>
                    <Link to="/cart ">
                      <img
                        src={CART}
                        className="w-[32px] h-[32px]"
                        alt="Billfold Indonesia"
                      />
                    </Link>
                  </div>

                  {cartCount == 0 ? null : (
                    <div className="bg-red-500 w-32 h-32 flex flex-col items-center justify-center rounded-full absolute top-[-8px] right-[-8px]">
                      <p className="text-white font-semibold text-13">
                        {cartCount > 99 ? "99+" : cartCount}
                      </p>
                    </div>
                  )}
                </div>

                {!Cookies.get("@token") ? (
                  <Link
                    to="/login"
                    className="py-8 bg-green-500 px-16 rounded-full"
                  >
                    <p className="font-Regular text-white">Masuk</p>
                  </Link>
                ) : (
                  <button
                    onClick={() => handleLogout()}
                    className="py-8 bg-red-100 px-16 rounded-full"
                  >
                    <p className=" font-SemiBold text-red-500">Keluar</p>
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      ) : null}

      <Routes>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/reset" element={<ResetPasswordPage />} />
        <Route element={<RequireAuth />}>
          <Route path="/" element={<HomePage />} />
          <Route path="/cart" element={<CartPage />} />
          <Route path="/topup" element={<TopupPage />} />
          <Route path="/withdrawal" element={<WithdrawalPage />} />
          <Route path="/transfer" element={<TransferPage />} />
          <Route path="/history" element={<HistoryTransactionPage />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
