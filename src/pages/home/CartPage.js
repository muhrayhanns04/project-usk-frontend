import { BASE_URL } from "../../helpers/Server";
import CartItem from "../../components/home/CartItem";
import Cookies from "js-cookie";
import NOTFOUND from "../../assets/icons/transaction/not-found.svg";
import React from "react";
import { SpinnerCircularFixed } from "spinners-react";
import Swal from "sweetalert2";
import axios from "axios";
import { formatNumberToRupiah } from "../../helpers/Common";
import { useNavigate } from "react-router-dom";
import { TransactionHooks } from "../../hooks/transactionhooks";

function CartPage() {
  const [isLoading, setIsLoading] = React.useState(false);
  const [products, setProducts] = React.useState(null);
  const [total, setTotal] = React.useState(0);
  const navigate = useNavigate();
  const { getCartCount } = TransactionHooks();

  const getCartProducts = async () => {
    try {
      const token = Cookies.get("@token");
      const response = await axios.get(`${BASE_URL}/transaction/oncart`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
        },
      });

      return setProducts(response.data.result);
    } catch (error) {
      console.log(error?.response?.data);
    }
  };

  const getTotalPayment = async () => {
    try {
      const token = Cookies.get("@token");
      const response = await axios.get(`${BASE_URL}/transaction/total`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
        },
      });

      return setTotal(response.data.result);
    } catch (error) {
      console.log(error?.response?.data);
    }
  };

  const handleCheckout = async () => {
    try {
      setIsLoading(true);
      const token = Cookies.get("@token");
      await axios.post(
        `${BASE_URL}/transaction/checkout`,
        {},
        {
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
          },
        }
      );

      getCartCount();
      setIsLoading(false);

      navigate("/", { replace: true });

      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener("mouseenter", Swal.stopTimer);
          toast.addEventListener("mouseleave", Swal.resumeTimer);
        },
      });

      Toast.fire({
        icon: "success",
        title: `Berhasil Menunggu Konfirmasi Penjual`,
      });
    } catch (error) {
      setIsLoading(false);
      Swal.fire({
        icon: "error",
        title: "Gagal",
        text: error?.response?.data?.message,
      });
    }
  };

  React.useEffect(() => {
    getCartProducts();
    getTotalPayment();
  }, []);

  return (
    <div className="flex flex-col justify-center items-center w-full md:bg-green-500  md:h-[100vh]">
      <div className="w-full bg-white md:h-[100%] md:w-[414px] overflow-hidden mb-[150px]">
        <div
          className={`md:h-[100%] scroll-py-10 overflow-auto flex flex-row flex-wrap justify-start items-start w-full ${
            products?.length < 1 ? "justify-center items-center" : null
          } md:w-[414px]`}
        >
          {products?.map((item, index) => {
            return (
              <CartItem
                item={item}
                index={index}
                getCartProducts={() => getCartProducts()}
                getTotalPayment={() => getTotalPayment()}
              />
            );
          })}

          {products?.length < 1 ? (
            <div className="mt-24  flex flex-col justify-center items-center">
              <img src={NOTFOUND} alt="tidak ditemukan" className="mb-16" />
              <p className="font-Bold text-gray-800 text-18 text-center my-8 ">
                Kosong
              </p>
              <p className="font-Regular text-gray-800 text-16 text-center w-[300px]">
                Belum ada barang yang ditambahkan kedalam keranjang
              </p>
            </div>
          ) : null}
        </div>
      </div>

      <div className="flex flex-col fixed bottom-0 bg-gray-50 w-full py-13 px-13 md:w-[414px] pb-[48px]">
        <p className="font-Regular text-gray-800 text-center">Total</p>

        <p className="font-Bold text-gray-800 text-24 text-center mb-13">
          {formatNumberToRupiah(total)}
        </p>

        <button
          disabled={total == 0 ? true : false}
          onClick={() => handleCheckout()}
          className="mt-16 rounded-full bg-green-500 focus:ring-4 h-[52px] w-full ring-green-400 flex flex-col justify-center items-center"
        >
          {isLoading ? (
            <SpinnerCircularFixed
              size={16}
              thickness={180}
              speed={180}
              color="#36ad47"
              secondaryColor="rgba(0, 0, 0, 0.44)"
            />
          ) : (
            <h6 className="font-SemiBold text-13 my-8 text-white">Bayar</h6>
          )}
        </button>
      </div>
    </div>
  );
}

export default CartPage;
