import { BASE_URL } from "../../helpers/Server";
import Cookies from "js-cookie";
import HISTORY from "../../assets/icons/home/directbox-notif.svg";
import ProductItem from "../../components/home/ProductItem";
import React from "react";
import TOPUP from "../../assets/icons/home/topup.svg";
import { ReactComponent as TRANSFER } from "../../assets/icons/home/arrow-swap-horizontal.svg";
import TransactionHomeItem from "../../components/transaction/TransactionHomeItem";
import WITHDRAWAL from "../../assets/icons/home/withdrawal.svg";
import axios from "axios";
import { formatNumberToRupiah } from "../../helpers/Common";

function HomePage() {
  const [products, setProducts] = React.useState(null);
  const [profile, setProfile] = React.useState(null);

  const getProducts = async () => {
    try {
      const token = Cookies.get("@token");
      const response = await axios.get(`${BASE_URL}/products`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
        },
      });

      return setProducts(response.data.result);
    } catch (error) {
      console.log(error?.response?.data);
    }
  };

  const getProfileInfo = async () => {
    try {
      const token = Cookies.get("@token");
      const response = await axios.get(`${BASE_URL}/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
        },
      });
      return setProfile(response.data.result);
    } catch (error) {
      console.log(error?.response?.data);
    }
  };

  React.useEffect(() => {
    getProducts();
    getProfileInfo();
  }, []);

  return (
    <>
      <div className="flex flex-col justify-center items-center md:bg-green-500 w-full md:h-[100vh] pb-[30px]">
        <div className="w-full md:w-[414px] bg-white flex flex-col items-center justify-center">
          <p className="font-Regular  text-gray-800  text-13 text-center">
            Saldo
          </p>

          <h4 className="font-Bold text-gray-800 text-24 text-center my-8">
            {formatNumberToRupiah(profile?.saldo ? profile?.saldo : 0)}
          </h4>

          <div className="flex flex-row items-center mt-16 mb-24">
            <TransactionHomeItem
              icon={TOPUP}
              title="Topup"
              style="mr-32"
              path="/topup"
            />
            <TransactionHomeItem
              icon={WITHDRAWAL}
              title="Tarik Tunai"
              style="mr-32"
              path="/withdrawal"
            />
            <TransactionHomeItem
              icon={<TRANSFER stroke="#FFFFFF" />}
              type="ci"
              title="Transfer"
              style="mr-32"
              path="/transfer"
            />
            <TransactionHomeItem
              icon={HISTORY}
              title="Histori Transaki"
              path="/history"
            />
          </div>
        </div>

        <div className="w-full md:w-[414px] bg-white md:h-[100vh]">
          <div
            className={`md:w-[414px] flex flex-row flex-wrap justify-start items-start w-full ${
              products?.length < 1 ? "justify-center items-center" : null
            } `}
          >
            {products?.map((item, index) => {
              return (
                <React.Fragment key={index}>
                  <ProductItem item={item} getProducts={() => getProducts()} />
                </React.Fragment>
              );
            })}

            {products?.length < 1 ? (
              <div className="mt-24">
                <p className="font-Bold text-gray-800 text-18 text-center my-8">
                  Kosong
                </p>
                <p className="font-Regular text-gray-800 text-16 text-center w-[300px]">
                  Belum ada barang yang dijual, silahkan hubungi pihak sekolah
                </p>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
}

export default HomePage;
