import { Dimmer, Loader } from "semantic-ui-react";

import { BASE_URL } from "../../helpers/Server";
import Cookies from "js-cookie";
import KEY from "../../assets/icons/auth/key.svg";
import LOGO from "../../assets/icons/billfold-logo-large.svg";
import React from "react";
import USER from "../../assets/icons/auth/user.svg";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function LoginPage() {
	const [auth, setAuth] = React.useState({
		nisn: null,
		password: null,
	});
	const [errors, setErrors] = React.useState(null);
	const [isLoading, setIsLoading] = React.useState(false);
	const navigate = useNavigate();

	const handleLogin = async () => {
		try {
			setIsLoading(true);
			const response = await axios.post(`${BASE_URL}/login`, {
				level: "student",
				password: auth.password,
				registration_number: auth.nisn,
			});
			Cookies.set("@token", response.data.token);
			Cookies.set("@id", response.data.results.id);
			setErrors(null);
			setTimeout(() => {
				setIsLoading(false);
			}, 500);
			navigate("/", { replace: true });
		} catch (error) {
			setTimeout(() => {
				setIsLoading(false);
			}, 500);
			setErrors(error?.response?.data?.message);
		}
	};
	return (
		<div className="flex h-[100vh] flex-col justify-start items-center w-full bg-white">
			{isLoading ? (
				<Dimmer active>
					<Loader size="mini">Loading</Loader>
				</Dimmer>
			) : null}
			<div className="flex flex-col w-90%  py-16">
				<div className="mt-24">
					<div className="flex flex-col justify-center items-center">
						<img
							src={LOGO}
							alt="password"
							className="self-center mb-16"
						/>
						<p className="font-Regular text-gray-800 text-center text-13">
							Pembayaran mudah menggunakan Billfold
						</p>
					</div>

					<div className="mt-42">
						<div className="flex flex-row items-center relative mb-16">
							<img
								src={USER}
								alt="user"
								className="absolute left-16"
							/>
							<input
								value={auth.nisn}
								onChange={(v) =>
									setAuth({ ...auth, nisn: v.target.value })
								}
								placeholder="Masukkan NISN"
								type="number"
								className="font-Regular text-black px-16 py-16 w-full bg-gray-50 outline-none rounded-16 pl-[52px]"
							/>
						</div>
						<div className="flex flex-row items-center relative">
							<img
								src={KEY}
								alt="password"
								className="absolute left-16"
							/>
							<input
								value={auth.password}
								onChange={(v) =>
									setAuth({
										...auth,
										password: v.target.value,
									})
								}
								placeholder="Masukkan Password"
								type="password"
								className="font-Regular text-black px-16 py-16 w-full bg-gray-50 outline-none rounded-16 pl-[52px]"
							/>
						</div>

						<button
							className="rounded-full bg-green-500 focus:ring-4 py-[20px] w-full ring-green-200 mt-16"
							onClick={() => handleLogin()}
						>
							<h6 className="font-SemiBold text-13 text-gray-100">
								Masuk
							</h6>
						</button>

						{errors !== null ? (
							<div className="bg-red-100 p-16 mt-16 rounded-8">
								<div className="flex flex-row items-center">
									<div className="w-8 h-8 bg-gray-800 rounded-full mr-8"></div>
									<p className="font-Regular text-13 text-gray-800">
										{errors}
									</p>
								</div>
							</div>
						) : null}
					</div>
				</div>
			</div>
		</div>
	);
}

export default LoginPage;
