import { BASE_URL } from "../../helpers/Server";
import Cookies from "js-cookie";
import MONEY from "../../assets/icons/transaction/money.svg";
import React from "react";
import { SpinnerCircularFixed } from "spinners-react";
import Swal from "sweetalert2";
import axios from "axios";
import { formatNumberToRupiah } from "../../helpers/Common";

function TopupPage() {
	const [profile, setProfile] = React.useState(null);
	const [request, setRequest] = React.useState(null);
	const [isLoading, setIsLoading] = React.useState(false);

	const getProfileInfo = async () => {
		try {
			const token = Cookies.get("@token");
			const response = await axios.get(`${BASE_URL}/profile`, {
				headers: {
					Authorization: `Bearer ${token}`,
					Accept: "application/json",
				},
			});
			return setProfile(response.data.result);
		} catch (error) {
			console.log(error?.response?.data);
		}
	};

	const handleTopup = async () => {
		try {
			setIsLoading(true);
			const token = Cookies.get("@token");
			const response = await axios.post(
				`${BASE_URL}/transaction/topup`,
				{
					cash_request: request,
				},
				{
					headers: {
						Authorization: `Bearer ${token}`,
						Accept: "application/json",
					},
				}
			);

			setTimeout(() => {
				setIsLoading(false);
			}, 500);
			const Toast = Swal.mixin({
				toast: true,
				position: "top-end",
				showConfirmButton: false,
				timer: 1500,
				timerProgressBar: true,
				didOpen: (toast) => {
					toast.addEventListener("mouseenter", Swal.stopTimer);
					toast.addEventListener("mouseleave", Swal.resumeTimer);
				},
			});

			Toast.fire({
				icon: "success",
				title: `Berhasil Menunggu Konfirmasi Bank SMKN 10 JAKARTA`,
			});

			getProfileInfo();
		} catch (error) {
			setTimeout(() => {
				setIsLoading(false);
			}, 500);
			Swal.fire({
				icon: "error",
				title: "Gagal",
				text: error?.response?.data?.message,
			});
		}
	};

	React.useEffect(() => {
		getProfileInfo();
	}, []);

	return (
		<div className="flex flex-col justify-center items-center smd:bg-green-500 w-full md:h-[100vh]">
			<div className="w-full md:w-[414px] bg-white flex flex-col items-center justify-center">
				<p className="font-Regular  text-gray-800 text-13 text-center">
					Saldo
				</p>

				<h4 className="font-Bold  text-gray-800 text-24 text-center my-8">
					{formatNumberToRupiah(profile?.saldo ? profile?.saldo : 0)}
				</h4>

				<div className="w-full md:w-[414px] px-16 mt-24">
					<div className="flex flex-row items-center relative mb-8">
						<img
							src={MONEY}
							alt="user"
							className="absolute left-16"
						/>
						<input
							value={request}
							onChange={(v) => setRequest(v.target.value)}
							placeholder="Masukkan Jumlah Topup"
							type="number"
							className="font-Regular text-black px-16 py-16 w-full bg-gray-50 outline-none rounded-16 pl-[52px]"
						/>
					</div>

					{request ? (
						<p className="font-SemiBold text-left text-red-500 text-13">
							REQUEST: {formatNumberToRupiah(request)}
						</p>
					) : null}

					<button
						disabled={
							request == null || request == 0 || request == ""
								? true
								: false
						}
						onClick={() => handleTopup()}
						className="mt-16 rounded-full bg-green-500 focus:ring-4 h-[52px] w-full ring-green-400 flex flex-col justify-center items-center"
					>
						{isLoading ? (
							<SpinnerCircularFixed
								size={16}
								thickness={180}
								speed={180}
								color="#36ad47"
								secondaryColor="rgba(0, 0, 0, 0.44)"
							/>
						) : (
							<h6 className="font-SemiBold text-13 my-8 text-white">
								Topup
							</h6>
						)}
					</button>
				</div>
			</div>
		</div>
	);
}

export default TopupPage;
