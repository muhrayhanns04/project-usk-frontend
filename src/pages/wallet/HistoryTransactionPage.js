import { BASE_URL } from "../../helpers/Server";
import Cookies from "js-cookie";
import NOTFOUND from "../../assets/icons/transaction/not-found.svg";
import React from "react";
import TopupItem from "../../components/TopupItem";
import TransactionItem from "../../components/TransactionItem";
import TransferItem from "../../components/TransferItem";
import WithdrawalItem from "../../components/WithdrawalItem";
import axios from "axios";

function HistoryTransactionPage() {
  const [type, setType] = React.useState({
    id: 0,
    name: "Produk",
  });
  const [status, setStatus] = React.useState({
    id: 0,
    name: "Menunggu",
  });

  const [data, setData] = React.useState(null);

  const types = ["Produk", "Topup", "Withdrawal", "Transfer"];
  const statuses = [
    {
      id: 0,
      name: "Menunggu",
    },
    {
      id: 1,
      name: "Ditolak",
    },
    {
      id: 2,
      name: "Selesai",
    },
  ];

  const getHistory = async (status, type) => {
    try {
      const token = Cookies.get("@token");
      const response = await axios.get(
        `${BASE_URL}/transaction/history/${Cookies.get(
          "@id"
        )}?status=${status}&type=${type}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: "application/json",
          },
        }
      );
      setData(response?.data?.result);
    } catch (error) {
      console.log(error?.response?.data);
    }
  };

  React.useEffect(() => {
    getHistory(0, 3);
  }, []);

  return (
    <div className="flex flex-col justify-center items-center md:bg-green-500 w-full md:h-[100vh]">
      <div className="w-full md:w-[414px] bg-white flex flex-col items-center justify-center">
        <h4 className="font-Bold text-gray-800 text-24 text-center my-8">
          Histori Transaksi
        </h4>

        {/* Type */}
        <div className="flex flex-row items-center mt-16">
          {types.map((item, index) => {
            return (
              <div
                key={index}
                onClick={() => {
                  setType({ id: index, name: item });
                  setStatus({
                    id: 0,
                    name: "Menunggu",
                  });

                  if (item == "Topup") {
                    return getHistory(0, 0);
                  } else if (item == "Produk") {
                    return getHistory(0, 3);
                  } else if (item == "Withdrawal") {
                    return getHistory(0, 1);
                  } else if (item == "Transfer") {
                    return getHistory(0, 4);
                  }
                }}
                className={`py-8 px-16 rounded-full ${
                  index + 1 == types.length ? "" : "mr-16"
                } ${type.id == index ? "bg-green-500" : ""} ${
                  type.id == index ? "" : "border-2 border-green-500"
                }`}
              >
                <p
                  className={`font-bold  text-16 text-center ${
                    type.id == index ? "text-white" : "text-green-500"
                  }`}
                >
                  {item}
                </p>
              </div>
            );
          })}
        </div>

        {/* Status */}
        <div className="flex flex-row items-center my-16">
          {statuses.map((item, index) => {
            return (
              <div
                key={index}
                onClick={() => {
                  setStatus({ id: item.id, name: item.name });
                  if (type?.name == "Topup") {
                    return getHistory(item?.id, 0);
                  } else if (type?.name == "Produk") {
                    return getHistory(item?.id, 3);
                  } else if (type?.name == "Withdrawal") {
                    return getHistory(item?.id, 1);
                  } else if (type?.name == "Transfer") {
                    return getHistory(item?.id, 4);
                  }
                }}
                className={`py-8 px-16 ${
                  status.id == index ? "border-b-2 border-b-green-500" : ""
                } ${index + 1 == statuses.length ? "" : "mr-16"}`}
              >
                <p
                  className={`font-bold  text-16 text-center ${
                    status.id == index ? "text-green-500" : "text-gray-800"
                  }`}
                >
                  {item.name}
                </p>
              </div>
            );
          })}
        </div>

        {data?.length < 1 || data == null ? (
          <div>
            <img src={NOTFOUND} alt="tidak ditemukan" className="mb-16" />
            <h2 className="font-Bold text-18 text-gray-800 mb-8 text-center">
              Transaksi tidak ditemukan
            </h2>
            <p className=" font-SemiBold text-13 text-gray-500 text-center ">
              Segera order beberapa barang dan nikmati gratis poinnya
            </p>
          </div>
        ) : null}

        {type.name === "Produk"
          ? data?.map((item, index) => {
              return (
                <React.Fragment key={index}>
                  <TransactionItem item={item} index={index} />
                </React.Fragment>
              );
            })
          : null}
        {type.name === "Topup"
          ? data?.map((item, index) => {
              return (
                <React.Fragment key={index}>
                  <TopupItem item={item} index={index} />
                </React.Fragment>
              );
            })
          : null}
        {type.name === "Withdrawal"
          ? data?.map((item, index) => {
              return (
                <React.Fragment key={index}>
                  <WithdrawalItem item={item} index={index} />
                </React.Fragment>
              );
            })
          : null}
        {type.name === "Transfer"
          ? data?.map((item, index) => {
              return (
                <React.Fragment key={index}>
                  <TransferItem item={item} index={index} />
                </React.Fragment>
              );
            })
          : null}
      </div>
    </div>
  );
}

export default HistoryTransactionPage;
