import { BASE_URL } from "../helpers/Server";
import Cookies from "js-cookie";
import React from "react";
import { TransactionContext } from "../context/TransactionContext";
import axios from "axios";

export const TransactionHooks = () => {
	const { cartCount, setCartCount } = React.useContext(TransactionContext);

	const getCartCount = async () => {
		try {
			const token = Cookies.get("@token");
			const response = await axios.get(
				`${BASE_URL}/transaction/cartcount`,
				{
					headers: {
						Authorization: `Bearer ${token}`,
						Accept: "application/json",
					},
				}
			);

			return setCartCount(response.data.result);
		} catch (error) {
			console.log(error?.response?.data);
		}
	};

	return {
		getCartCount,
		cartCount,
	};
};
